package com.admin1x2.controller.javafx;

public enum View {
	PRINCIPAL(
		"view/javafx/Principal.fxml",
		"admin1x2"
	),
	TIPOS_EQUIPO(
		"view/javafx/TiposEquipo.fxml",
		"Tipos de equipo"
	);
	
	private String fxml;
	private String title;
	
	public String getFxml(){
		return fxml;
	}
	
	public String getTitle(){
		return title;
	}
	
	View(String fxml, String title){
		this.fxml = fxml;
		this.title = title;
	}	
}
