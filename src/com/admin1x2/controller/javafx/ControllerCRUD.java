package com.admin1x2.controller.javafx;

public abstract class ControllerCRUD extends Controller {

	protected StateCRUD state;
	
	public ControllerCRUD(){
		super();
	}
	
	protected abstract void setState(StateCRUD state);
}
