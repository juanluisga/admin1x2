package com.admin1x2.controller.javafx;

public enum StateCRUD {
	CREATE,
	READ,
	UPDATE,
	DELETE
}
