package com.admin1x2.controller.javafx;

import java.io.IOException;

import com.admin1x2.MainAppFx;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public abstract class Controller {
	
	private static MainAppFx mainApp;
	private Stage stage;
	
	/*
	 * GETTERS & SETTERS
	 */
	public final void setMainApp(MainAppFx mainApp){
		Controller.mainApp = mainApp;
	}
	
	public final MainAppFx getMainApp(){
		return Controller.mainApp;
	}
	
	/*
	 * MÉTODOS PRIVADOS
	 */
    public void openScene(View view){
	    try {
	        // Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(MainAppFx.class.getResource(view.getFxml()));
	        AnchorPane page = (AnchorPane) loader.load();

	        // Create the dialog Stage.
	        this.stage = new Stage();
	        this.stage.setTitle(view.getTitle());
	        this.stage.setResizable(false);
	        this.stage.initModality(Modality.WINDOW_MODAL);
	        this.stage.initOwner(this.getMainApp().getPrimaryStage());
	        Scene scene = new Scene(page);
	        this.stage.setScene(scene);

	        // Set the person into the controller.
	        //TiposEquipo controller = loader.getController();
	        //controller.setDialogStage(dialogStage);
	        //controller.setPerson(person);

	        // Show the dialog and wait until the user closes it
	        stage.showAndWait();
	        
	        //return controller.isOkClicked();
	    } catch (IOException e) {
	        e.printStackTrace();
	        //return false;
	    }	
    }

    public void visible(boolean visible){
    	if (visible) 
    		this.stage.show();
    	else 
    		this.stage.hide();
    }
	
}	
