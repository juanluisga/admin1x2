package com.admin1x2.controller.javafx;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.admin1x2.model.javafx.TipoEquipoFx;
import com.admin1x2.model.mybatis.MyBatisUtil;
import com.admin1x2.model.mybatis.pojos.TipoEquipo;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;

public class TiposEquiposController extends ControllerCRUD {

	@FXML
    private TableView<TipoEquipoFx>tvTipoEquipo;
    
    @FXML
    private TableColumn<TipoEquipoFx, Number> tcTeId;
    
    @FXML
    private TableColumn<TipoEquipoFx, String> tcTeDenominacion;
    
    @FXML
    private Label lblTipo;
    
    @FXML
    private TextField txtTipo;
    
    @FXML
    private Group grupoDatos;
    
    @FXML
    private Button btnAceptar;
    
    @FXML
    private Button btnCancelar;
    
    @FXML
    private HBox botonesConfirmacion;
    
    @FXML
    private Button btnNuevo;
    
    @FXML
    private Button btnEditar;
    
    @FXML
    private Button btnEliminar;
    
    @FXML
    private HBox botonesCRUD;
    
    
    private ObservableList<TipoEquipoFx> lstTipoEquipo = FXCollections.observableArrayList();
    
    /*
     * CONSTRUCTORES
     */
	public TiposEquiposController(){
		super();
	}
	
    @FXML
    public void initialize() {
    	inicializarControles();
    	getTiposEquipo(this.lstTipoEquipo);
    }

    /*
     * EVENTOS
     */
	@FXML
	private void btnNuevo_click(ActionEvent event){
		setState(StateCRUD.CREATE);
	}
    
	@FXML
	private void btnEditar_click(ActionEvent event){
		if (elementoSeleccionado()){
			setState(StateCRUD.UPDATE);
		}
		
	}
	
	@FXML
	private void btnEliminar_click(ActionEvent event){
		if (elementoSeleccionado()){
			setState(StateCRUD.DELETE);
		}
 	}
	
	@FXML
	private void btnAceptar_click(ActionEvent event){
		aceptar();
	}
	
	@FXML
	private void btnCancelar_click(ActionEvent event){
		cancelar();
	}
	
	@FXML
	private void tcTeDenominacion_EditCommit(CellEditEvent<TipoEquipoFx, String> event){
		event.getTableView().getItems().get(event.getTablePosition().getRow()).setTe_denominacion(event.getNewValue());
		setState(StateCRUD.READ);
	}
	
	
	/*
	 * MÉTODOS PRIVADOS
	 */
	
	protected void setState(StateCRUD estado){
		switch(estado){
			case CREATE:
				this.state = StateCRUD.CREATE;
				botonesCRUD.setVisible(false);
				grupoDatos.setDisable(false);
				txtTipo.clear();
				botonesConfirmacion.setVisible(true);
				btnAceptar.setText("Crear");
				break;
				
			case READ:
				this.state = StateCRUD.READ;
				botonesCRUD.setVisible(true);
				grupoDatos.setDisable(true);
				botonesConfirmacion.setVisible(false);
				break;
				
			case UPDATE:
				this.state = StateCRUD.UPDATE;
				botonesCRUD.setVisible(false);
				grupoDatos.setDisable(false);
				botonesConfirmacion.setVisible(true);
				btnAceptar.setText("Editar");
				break;
				
			case DELETE:
				this.state = StateCRUD.DELETE;
				botonesCRUD.setVisible(false);
				grupoDatos.setDisable(true);
				botonesConfirmacion.setVisible(true);
				btnAceptar.setText("Borrar");
				break;
		}
	}
	
	protected StateCRUD getState(){
		return this.state;
	}
	
	private void inicializarControles(){
		
		/*
		 * ESTADO
		 */
		setState(StateCRUD.READ);
		
		/*
		 * BOTONES
		 */
		// Eventos
		btnNuevo.setOnAction(this::btnNuevo_click);
		btnEditar.setOnAction(this::btnEditar_click);
		btnEliminar.setOnAction(this::btnEliminar_click);
		btnAceptar.setOnAction(this::btnAceptar_click);
		btnCancelar.setOnAction(this::btnCancelar_click);

		/*
		 * TABLA
		 */
		tcTeId.setCellValueFactory(cellData -> cellData.getValue().te_idProperty());
		tcTeDenominacion.setCellValueFactory(cellData -> cellData.getValue().te_denominacionProperty());
		tcTeDenominacion.setCellFactory(TextFieldTableCell.forTableColumn());
		tcTeDenominacion.setOnEditCommit(this::tcTeDenominacion_EditCommit);
		
		// Listen for selection changes and show the person details when changed.
		tvTipoEquipo.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> mostrarDatos(newValue));
		
		tvTipoEquipo.setItems(lstTipoEquipo);		
		
	}

	private void getTiposEquipo(ObservableList<TipoEquipoFx> listaFx){
		listaFx.clear();
		List<TipoEquipo> lista = null;
		SqlSession session = MyBatisUtil.getSession();
		if (session != null){
			try{
				lista = session.selectList("TipoEquipo.getTiposEquipo");
				for (TipoEquipo te : lista){
					listaFx.add(new TipoEquipoFx(te.getTe_id() , te.getTe_denominacion()));
				}
			} finally {
				session.close();
			}
		} else {
			System.out.println("Error");
		}
	}
	
	private void mostrarDatos(TipoEquipoFx tipoEquipo){
		if ( tipoEquipo == null ){
			txtTipo.setText("");
		} else {
			txtTipo.setText(tipoEquipo.getTe_denominacion());
		}
	}


	private void aceptar(){
		switch ( getState() ){
			case CREATE:
				nuevo();
				break;
			case READ:
				;
				break;
			case UPDATE:
				actualizar();
				break;
			case DELETE:
				eliminar();
				break;
		}
		getTiposEquipo(this.lstTipoEquipo);
	}
	
	private void cancelar(){
		setState(StateCRUD.READ);
	}

	private void nuevo(){
		SqlSession session = MyBatisUtil.getSession();
		if (session != null){
			try {
				session.insert("TipoEquipo.nuevoTipoEquipo",new TipoEquipoFx(txtTipo.getText()));
			} finally {
				session.close();
			}
		} else {
			System.out.println("Error");
		}
	}
	
	private void actualizar(){
		SqlSession session = MyBatisUtil.getSession();
		if (session != null){
			try {
				TipoEquipoFx te = tvTipoEquipo.getSelectionModel().getSelectedItem();
				te.setTe_denominacion(txtTipo.getText());
				session.update("TipoEquipo.actualizarTipoEquipo", te );
			} finally {
				session.close();
			}
		} else {
			System.out.println("Error");
		}
	}
	
	private void eliminar(){
		SqlSession session = MyBatisUtil.getSession();
		if (session != null){
			try {
				session.delete("TipoEquipo.eliminarTipoEquipo", tvTipoEquipo.getSelectionModel().getSelectedItem());
			} finally {
				session.close();
			}
		} else {
			System.out.print("Error");
		}
	}
	
	private boolean elementoSeleccionado(){
		if ( tvTipoEquipo.getSelectionModel().getSelectedItem() == null ){
			return false;
		} else {
			return true;
		}
	}
}
