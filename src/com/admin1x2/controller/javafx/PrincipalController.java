package com.admin1x2.controller.javafx;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.StageStyle;

public class PrincipalController extends Controller{
	
	
	/*
	 * DECLARACIÓN CONTROLADORES 
	 */
	private TiposEquiposController tec;
	
	@FXML
	public void exit(){
		System.exit(0);
	}
	
	@FXML
	public void creditos(){
    	Alert cartel = new Alert(Alert.AlertType.INFORMATION);
    	cartel.initStyle(StageStyle.UNDECORATED);
    	cartel.setResult(ButtonType.OK);
    	cartel.setTitle("Créditos");
    	cartel.setHeaderText("Administrador guru1x2");
    	cartel.setContentText("Autor: Juan Luis García");
    	cartel.showAndWait();		
	}

	
	@FXML
	public void abrir(){
		FileChooser elegirFichero = new FileChooser();
		
		// definir extensión del fichero
		ExtensionFilter extension = new ExtensionFilter("SQLite (*.db)", "*.db");
		elegirFichero.getExtensionFilters().add(extension);
		extension = new ExtensionFilter("Base de datos guru (*.gdb", "*.gdb");
		elegirFichero.getExtensionFilters().add(extension);
		
		// mostrar diálogo
		elegirFichero.showOpenDialog(this.getMainApp().getPrimaryStage());
		
		
	}
	
	@FXML
	public void tiposEquipo(){
		if ( tec == null){
			tec = new TiposEquiposController();
			tec.openScene(View.TIPOS_EQUIPO);
		} else {
			tec.visible(true);
		}
		

	}
	
}
