package com.admin1x2;

import java.io.IOException;

import com.admin1x2.controller.javafx.PrincipalController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainAppFx extends Application {

	private Stage stagePrincipal;
	private BorderPane panelPrincipal;
	
	@Override
	public void start(Stage primaryStage) {
		this.stagePrincipal = primaryStage;
		this.stagePrincipal.setTitle("Administrador guru1x2");
		
		initPanelPrincipal();
		
	}
	
	/*
	 * Iniciar panel principal
	 */
	private void initPanelPrincipal(){
		try{
			// cargar el panel desde fichero fxml
			FXMLLoader loader = new FXMLLoader();
			//cargador.setLocation(MainAppFx.class.getResource("vista/Principal.fxml"));
			loader.setLocation(this.getClass().getResource("view/javafx/Principal.fxml"));
			this.panelPrincipal = (BorderPane) loader.load();
			
			// Mostrar la escena conteniendo el panel principal
			Scene escena = new Scene(panelPrincipal);
			this.stagePrincipal.setScene(escena);
			
			// Pasar 
			
	        PrincipalController controlador = loader.getController();
	        controlador.setMainApp(this);
			
			this.stagePrincipal.show();
		} catch (IOException ex){
			ex.printStackTrace();
		}
	}

	public Stage getPrimaryStage(){
		return this.stagePrincipal;
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
}
