package com.admin1x2.model.mybatis.pojos;

public class TipoEquipo {
	
	private int te_id;
	private String te_denominacion;
	
	public TipoEquipo(){
		
	}
	
	public TipoEquipo(int te_id, String te_denominacion){
		this.te_id = te_id;
		this.te_denominacion = te_denominacion;
	}

	public TipoEquipo(String te_denominacion){
		this.te_denominacion = te_denominacion;
	}

	public int getTe_id() {
		return te_id;
	}

	public void setTe_id(int te_id) {
		this.te_id = te_id;
	}

	public String getTe_denominacion() {
		return te_denominacion;
	}

	public void setTe_denominacion(String te_denominacion) {
		this.te_denominacion = te_denominacion;
	}

	@Override
	public String toString() {
		return "TipoEquipo [te_id=" + te_id + ", te_denominacion=" + te_denominacion + "]";
	}

	

	
	
	
}
