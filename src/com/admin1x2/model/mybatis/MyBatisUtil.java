package com.admin1x2.model.mybatis;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class MyBatisUtil {
	
	private static final String resource = "com/admin1x2/model/mybatis/mybatis-config.xml";
	private static SqlSession session = null;

	public static SqlSession getSession(){
		try{
			Reader reader = Resources.getResourceAsReader(MyBatisUtil.resource);
			SqlSessionFactory sqlMapper = new SqlSessionFactoryBuilder().build(reader);
			MyBatisUtil.session = sqlMapper.openSession();
		} catch(IOException ex){
			ex.printStackTrace();
		}
		return session;
	}
}
