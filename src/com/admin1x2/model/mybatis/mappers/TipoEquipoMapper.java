package com.admin1x2.model.mybatis.mappers;

import java.util.List;

import com.admin1x2.model.mybatis.pojos.TipoEquipo;

public interface TipoEquipoMapper {
	public void nuevoTipoEquipo(TipoEquipo tipoEquipo);
	public void actualizarTipoEquipo(TipoEquipo tipoEquipo);
	public void eliminarTipoEquipo(TipoEquipo tipoEquipo);
	public List<TipoEquipo> getTiposEquipo();
}
