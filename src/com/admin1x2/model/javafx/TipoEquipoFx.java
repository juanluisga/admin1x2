package com.admin1x2.model.javafx;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TipoEquipoFx {
	
	private IntegerProperty te_id;
	private StringProperty te_denominacion;
	
	
	public TipoEquipoFx(){
		
	}
	
	public TipoEquipoFx(int te_id, String te_denominacion){
		this.te_id = new SimpleIntegerProperty(te_id);
		this.te_denominacion = new SimpleStringProperty(te_denominacion);
	}

	public TipoEquipoFx(String te_denominacion){
		this.te_denominacion = new SimpleStringProperty(te_denominacion);
	}

	public int getTe_id() {
		return this.te_id.get();
	}

	public void setTe_id(int te_id) {
		//this.te_id.set(te_id);
		this.te_id.setValue(te_id);
	}
	
	public IntegerProperty te_idProperty(){
		return this.te_id;
	}
	
	public String getTe_denominacion() {
		return this.te_denominacion.get();
	}

	public void setTe_denominacion(String te_denominacion) {
		this.te_denominacion.set(te_denominacion);
	}
	
	public StringProperty te_denominacionProperty(){
		return this.te_denominacion;
	}


}
