#! /usr/bin/env python3
equipos = []
with open("../data/historico_liga.txt", "r", encoding="cp1252") as archivo_partidos:
    for partido in archivo_partidos: 
        local = partido[28:45].strip()
        visitante = partido[45:62].strip()
        if not local in equipos:
            equipos.append(local)
        if not visitante in equipos:
            equipos.append(visitante)
equipos.sort()
with open("../data/equipos.txt", "w") as archivo_equipos:
    for equipo in equipos:
        archivo_equipos.write(equipo + "\n")
