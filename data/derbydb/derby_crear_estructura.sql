CREATE TABLE TIPO_PARTIDO (
	tp_id			INTEGER 	
					NOT NULL 
					PRIMARY KEY 
					GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),
					
	tp_denominacion	VARCHAR(50) 
					NOT NULL
);

CREATE TABLE TIPO_EQUIPO (
	te_id			INTEGER 	
					NOT NULL 
					PRIMARY KEY
					GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),
					
	te_denominacion	VARCHAR(50) 
					NOT NULL
);

CREATE TABLE EQUIPO (
	eq_id		INTEGER
				NOT NULL 
				PRIMARY KEY
				GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),
				
	eq_nombre	VARCHAR(50) 
				NOT NULL,
				
	eq_tipo		INTEGER,
	
	FOREIGN KEY(eq_tipo) REFERENCES TIPO_EQUIPO(te_id)
);

CREATE TABLE TIPO_COMPETICION (
	tc_id			INTEGER 	
					NOT NULL 
					PRIMARY KEY
					GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),
					
	tc_denominacion	VARCHAR(50) 
					NOT NULL
);

CREATE TABLE COMPETICION (
	comp_id		INTEGER 	
				PRIMARY KEY
				GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),
				
	comp_nombre	VARCHAR(50) 
				NOT NULL,
				
	comp_tipo	INTEGER,
	
	FOREIGN KEY(comp_tipo) REFERENCES TIPO_COMPETICION(tc_id)
);

CREATE TABLE EDICION (
	ed_comp_id		INTEGER
					NOT NULL,
					
	ed_id			INTEGER
					NOT NULL
					GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),
					
	ed_denominacion	VARCHAR(50) 
					DEFAULT NULL,
					
	ed_sede			VARCHAR(50) 
					DEFAULT NULL,
					
	ed_fecha_inicio	VARCHAR(10) 
					DEFAULT NULL,
					
	ed_fecha_fin	VARCHAR(10) 
					DEFAULT NULL,
					
	PRIMARY KEY(ed_comp_id,ed_id),
	FOREIGN KEY(ed_comp_id) REFERENCES COMPETICION(comp_id)
);

CREATE TABLE PARTICIPANTE (
	comp_id		INTEGER 
				NOT NULL,
				
	edicion		INTEGER 
				NOT NULL,
				
	equipo_id	INTEGER 
				NOT NULL,
				
	PRIMARY KEY(comp_id,edicion,equipo_id),
	FOREIGN KEY(comp_id, edicion) REFERENCES EDICION(ed_comp_id, ed_id),
	FOREIGN KEY(equipo_id) REFERENCES EQUIPO(eq_id)
);

CREATE TABLE PARTIDO (
	partido_id			INTEGER
						NOT NULL 
						PRIMARY KEY
						GENERATED ALWAYS AS IDENTITY(START WITH 1, INCREMENT BY 1),
						
	comp_id				INTEGER 	
						NOT NULL,
						
	ed_id				INTEGER 	
						NOT NULL,
						
	partido_tipo		INTEGER,
	
	partido_num			INTEGER 	
						DEFAULT NULL,
						
	fecha				VARCHAR(10) 
						DEFAULT NULL,
						
	equipo_local		INTEGER 	
						NOT NULL,
						
	equipo_visitante	INTEGER 	
						NOT NULL,
						
	goles_local			INTEGER 	
						NOT NULL,
						
	goles_visitante		INTEGER 	
						NOT NULL,
						
	FOREIGN KEY(comp_id, ed_id) REFERENCES EDICION(ed_comp_id, ed_id),
	FOREIGN KEY(partido_tipo) REFERENCES TIPO_PARTIDO(tp_id),
	FOREIGN KEY(comp_id, ed_id, equipo_local) REFERENCES PARTICIPANTE(comp_id, edicion, equipo_id),
	FOREIGN KEY(comp_id, ed_id, equipo_visitante) REFERENCES PARTICIPANTE(comp_id, edicion, equipo_id)
);
	
COMMIT;